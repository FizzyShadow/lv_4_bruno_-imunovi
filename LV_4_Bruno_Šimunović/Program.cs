﻿using System;

namespace LV_4_Bruno_Šimunović
{
    class Program
    {


        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("file.csv");
            Analyzer3rdParty analyzer3RdParty = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer3RdParty);
            double[] array = adapter.CalculateAveragePerColumn(dataset);
            foreach (double element in array)
            {
                Console.WriteLine(element.ToString("#.##"));
            }

        }  
    }
}
