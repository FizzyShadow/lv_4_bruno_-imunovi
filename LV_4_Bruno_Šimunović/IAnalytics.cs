﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_4_Bruno_Šimunović
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
