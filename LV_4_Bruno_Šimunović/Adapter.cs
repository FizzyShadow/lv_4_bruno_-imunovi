﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LV_4_Bruno_Šimunović
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        public double[][] ConvertData(Dataset dataset)
        {
            double[][] arrays = dataset.GetData().Select(a => a.ToArray()).ToArray();
            return arrays;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
