﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LV_4_Bruno_Šimunović
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int columnCount = data[0].Length;
            int rowCount = data.Length;
            double[] results = new double[columnCount];
            for (int i = 0; i < columnCount; i++)
            {
                for (int j = 0; j < rowCount; j++)
                {
                    results[i] += data[j][i];
                }
                results[i] = results[i] / data.Length;
            }
            return results;
        }
    }
}
