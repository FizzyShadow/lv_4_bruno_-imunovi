﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6_7
{
    class Validation: IRegistrationValidator
    {
        private EmailValidator emailValidator;
        private PasswordValidator password;
        public Validation()
        {
            this.password = new PasswordValidator(10);
            this.emailValidator = new EmailValidator();
        }

        public bool IsUserEntryValid(UserEntry userEntry)
        {
            return emailValidator.IsValidAddress(userEntry.Email) && password.IsValidPassword(userEntry.Password);
        }

    }
}
