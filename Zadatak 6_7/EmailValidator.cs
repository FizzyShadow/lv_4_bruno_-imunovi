﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6_7
{
    class EmailValidator: IEmailValidatorService
    {
        public EmailValidator() { }
         
        
        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return AdressEndsWith(candidate)&&IsAtSignInAdress(candidate);
        }

        public bool AdressEndsWith(String candidate)
        {
            if (candidate.EndsWith(".hr"))
            {
                return true;
            }
            else if (candidate.EndsWith(".com"))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public bool IsAtSignInAdress(String candidate)
        {
           if (candidate.Contains("@"))
           {
                return true;
           }
           else
           {
                return false;
           }
          
        }
       
    }
}
