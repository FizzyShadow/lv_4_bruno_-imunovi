﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6_7
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
