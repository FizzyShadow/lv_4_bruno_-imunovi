﻿using System;

namespace Zadatak_6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            String Email = "simunovic1999@net.hr";
            String BadEmail = "simunovic1999net.hr";
            String Password = "MaliIva8ii";
            String BadPassword = "mrkva";

            EmailValidator emailValidator = new EmailValidator();
            PasswordValidator passwordValidator = new PasswordValidator(10);
            Console.WriteLine(emailValidator.IsValidAddress(Email));
            Console.WriteLine(emailValidator.IsValidAddress(BadEmail));
            Console.WriteLine("\n");
            Console.WriteLine(passwordValidator.IsValidPassword(Password));
            Console.WriteLine(passwordValidator.IsValidPassword(BadPassword));

            Console.WriteLine("\n");
            Validation validation = new Validation();

            while (!validation.IsUserEntryValid(UserEntry.ReadUserFromConsole())) ;
            
        }
    }
}
