﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6_7
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
