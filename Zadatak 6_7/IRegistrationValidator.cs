﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6_7
{
    interface IRegistrationValidator
    {
        bool IsUserEntryValid(UserEntry entry);
    }
}
