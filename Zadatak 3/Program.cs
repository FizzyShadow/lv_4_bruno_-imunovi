﻿using System;
using System.Collections.Generic;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            Book book = new Book("Svijet Atlantide");
            Video video = new Video("Exploring the universe");
            List<IRentable> rentables = new List<IRentable>();
            rentables.Add(book);
            rentables.Add(video);


            rentingConsolePrinter.PrintTotalPrice(rentables);
            rentingConsolePrinter.DisplayItems(rentables);

            
            IRentable rentable = new Book("Čuvari knjižnice");
            IRentable rentable2 = new Video("Matrix tutorial");
            HotItem hotItem = new HotItem(rentable);
            HotItem hotItem2 = new HotItem(rentable2);
            rentables.Add(hotItem);
            rentables.Add(hotItem2);

            Console.WriteLine("\n");
            rentingConsolePrinter.PrintTotalPrice(rentables);
            rentingConsolePrinter.DisplayItems(rentables);

            //Dolazi do dodavanja bounsa za knjigu i video

            Console.WriteLine("\n");
            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable element in rentables)
            {
                flashSale.Add(new DiscountedItem(element, 25));
            }

            rentingConsolePrinter.PrintTotalPrice(flashSale);
            rentingConsolePrinter.DisplayItems(flashSale);


        }
    }
}
