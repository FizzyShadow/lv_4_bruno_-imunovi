﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_3
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double DiscountForItem;
        public DiscountedItem(IRentable rentable, double discountForItem) : base(rentable) 
        {
            this.DiscountForItem = discountForItem;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice() * (this.DiscountForItem/100);
        }
        public override String Description
        {
            get
            {
                return "Now on [" + DiscountForItem + "]% off: " + base.Description ;
            }
        }
    }
}
